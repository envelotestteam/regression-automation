*** Test Cases ***
Wyslij list - check page display
    [Documentation]    Autor: Barbara Gromulska-Osojca
    ...    Środowisko: PRODUKCJA, UATIMP
    ...
    ...    Cel testu: Zweryfikowanie poprawności wyświetlania strony Wyślij list przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy.
    [Tags]    smoke    ok    prod    uatimp
    Maximize Browser Window
    ukBelka
    ukStopka
    Run Keyword If    '${wydanie}'=='2'    Click Link In Header    Wyślij list
    Comment    Run Keyword If    '${wydanie}'=='3'    Click Link    Wyślij list    # uwaga: w R3 nie ma "Wyślij List" z menu górnego
    Run Keyword If    '${wydanie}'=='2'    ukWyslij list - check page display    # sprawdzenie poprawności wyświetlania strony przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy

Neolist - check page display
    [Documentation]    Autor: Barbara Gromulska-Osojca
    ...    Środowisko: PRODUKCJA, UATIMP
    ...
    ...    Cel testu: Zweryfikowanie poprawności wyświetlania strony Neolist przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy.
    [Tags]    smoke    ok    prod    utimp
    Maximize Browser Window
    ukBelka
    ukStopka
    Click Link In Footer    Neolist
    ukWyslij list - check page display    # sprawdzenie poprawności wyświetlania strony przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy

Projekty - check page display
    [Documentation]    Autor: Barbara Gromulska-Osojca
    ...    Środowisko: PRODUKCJA, UATE1, UATIMP
    ...    Cel testu: Zweryfikowanie poprawności wyświetlania strony Projekty przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy.
    ...
    ...    UWAGA: na środowisku UAT nie działa - brak linku (stan na dzień 2014-07-24)
    ...    Na INTR3 nie ma linku (stan na 2014-09-05)
    [Tags]    smoke    ok    prod    uatimp
    Maximize Browser Window
    ukBelka
    ukStopka
    Run Keyword If    '${wydanie}'=='2'    Click Link In Footer    Projekty
    Run Keyword If    '${wydanie}'=='2'    ukZalogujSie - check page display    # sprawdzenie poprawności wyświetlania strony przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy - w sekcji logowanie
    Run Keyword If    '${wydanie}'=='2'    Run Keyword If    '${srodowisko}'!=''    Location Should Contain    ${srodowisko}
    Run Keyword If    '${wydanie}'=='2'    Location Should Contain    feppcas
    Comment    Run Keyword If    '${wydanie}'=='3'    Click Link In Footer    Projekty    # uwaga: na R3 nie ma "Projekty" w stopce

Archiwum- check page display
    [Documentation]    Autor: Barbara Gromulska-Osojca
    ...    Środowisko: PRODUKCJA, UATE1, UATIMP
    ...    Cel testu: Zweryfikowanie poprawności wyświetlania strony Archiwum przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy.
    ...
    ...    UWAGA: na środowisku UAT nie działa - brak podstrony (stan na dzień 2014-07-24)
    ...    Na INTR3 nie ma linku (stan na 2014-09-05)
    [Tags]    smoke    ok    prod    uatimp
    Maximize Browser Window
    ukBelka
    ukStopka
    Run Keyword If    '${wydanie}'=='2'    Click Link In Footer    Archiwum
    Run Keyword If    '${wydanie}'=='2'    ukZalogujSie - check page display    # sprawdzenie poprawności wyświetlania strony przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy - w sekcji logowanie
    Run Keyword If    '${wydanie}'=='2'    Run Keyword If    '${srodowisko}'!=''    Location Should Contain    ${srodowisko}
    Run Keyword If    '${wydanie}'=='2'    Location Should Contain    feppcas
    Comment    Run Keyword If    '${wydanie}'=='3'    Click Link In Footer    Archiwum    # na R3 nie ma Archiwum w stopce

Zmien haslo - check page display
    [Documentation]    Autor: Barbara Gromulska-Osojca
    ...    Środowisko: PRODUKCJA, \ UATIMP
    ...    Cel testu: Zweryfikowanie poprawności wyświetlania strony Zmień hasło przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy.
    ...
    ...    UWAGA: na środowisku UAT nie działa - brak podstrony (stan na dzień 2014-07-24)
    ...    Na INTR3 nie ma linku (stan na 2014-09-05)
    [Tags]    smoke    ok    prod    uatimp
    Maximize Browser Window
    ukBelka
    ukStopka
    Run Keyword If    '${wydanie}'=='2'    Click Link In Footer    Zmień hasło
    Run Keyword If    '${wydanie}'=='2'    ukZalogujSie - check page display    # sprawdzenie poprawności wyświetlania strony przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy - w sekcji logowanie
    Run Keyword If    '${wydanie}'=='2'    Run Keyword If    '${srodowisko}'!=''    Location Should Contain    ${srodowisko}
    Run Keyword If    '${wydanie}'=='2'    Location Should Contain    feppcas
    Comment    Run Keyword If    '${wydanie}'=='2'    Click Link In Footer    Zmień hasło    # na R3 nie ma "Zmień hasło"

Neorachunki Neofaktura na Wyslij kartke - check page display
    [Documentation]    Autor: Barbara Gromulska-Osojca
    ...    Środowisko: PRODUKCJA, UATE1, UAT, INTR3, UATIMP
    ...    Cel testu: Zweryfikowanie poprawności wyświetlania strony \ Neorachunki wywoływane ze strony "Wyślij kartkę" - przez zweryfikowanie, czy na strone widoczne są odpowiednie napisy.
    [Tags]    smoke    prod    ok    uate1    uat    intr3
    ...    uatimp
    Maximize Browser Window
    ukBelka
    ukStopka
    Run Keyword If    '${wydanie}'=='2'    Click Link In Header    Wyślij kartkę
    Run Keyword If    '${wydanie}'=='3'    Click Link    Wyślij kartkę
    Click Link In Footer    Neorachunki
    ukZalogujSie - check page display
    Run Keyword If    '${srodowisko}'!=''    Location Should Contain    ${srodowisko}
    Page Should Not Contain    There was an error
