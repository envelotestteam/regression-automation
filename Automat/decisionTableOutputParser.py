'''
Created on 2 cze 2014

@author: osojct01
'''


#from xml.dom import minidom
#import xml.etree.ElementTree as ET
from lxml import etree as ET
import sys
import os.path


class decisionTableOutputParser(object):
    
    def __init__(self, path):
        self.path = path
       # self.xmldoc = minidom.parse(self.path + "/parallel_output.xml")
        
    def decisionTablePars(self, source, target, for_iterator_name='testCaseNo'):
        output = ET.parse(source)
        czy = ET.iselement(output)
        kwForList = output.xpath("//kw[@type='for' and contains(@name,'%s')]" % for_iterator_name)
        for kwFor in kwForList:
            test = kwFor.xpath("./..")[0]
            testId = test.get("id").replace("t","s")
            kwList = test.xpath("./kw[@type='kw']")
            forItemList = kwFor.xpath("./kw[@type='foritem']")
            setup = ET.SubElement(test,"kw", type="setup", name="decision Table Setup")
            ET.SubElement(setup, "doc")
            ET.SubElement(setup, "arguments")
            setupStartTime = kwList[0].find("status").get("starttime")
            setupEndTime = kwList[-1].find("status").get("endtime")
            for kw in kwList:
                setup.append(kw)
            setupStatus = ET.SubElement(setup, "status", status="PASS")
            setupStatus.set("endtime",setupEndTime)
            setupStatus.set("starttime", setupStartTime)
            test.tag = "suite"
            test.set("id", testId)
            testNo=1
            for item in forItemList:
                item.tag = "test"
                item.set("id","%s-t%s" %(testId, testNo))
                testNo += 1
                item.remove(item.find("arguments"))
                test.append(item)
            test.remove(kwFor)
            test.append(test.find("kw"))
            test.append(test.find("status"))
            test.remove(test.find("tags"))
        output.write(target)

    def _childSuiteET(self, node):
            for child in node:
                if child.tag == 'suite':
                    return child
            return None

        
    def _childSuite(self, node):
            for child in node.childNodes:
                if child.nodeName == 'suite':
                    return child
            return None

    def _lastChildSuite(self, node):
            childReverse = node.childNodes
            childReverse.reverse()
            for child in childReverse:
                if child.nodeName == 'suite':
                    return child
            return None
           
       
            
if __name__ == "__main__":
    out = decisionTableOutputParser("gg")
    out.decisionTablePars(sys.argv[1],sys.argv[2], sys.argv[3])
